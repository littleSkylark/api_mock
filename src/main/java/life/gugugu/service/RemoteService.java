package life.gugugu.service;

import feign.Headers;
import feign.Param;
import feign.RequestLine;

/**
 * @author chances(YangPengLei)
 * @date 2021/1/22
 */
public interface RemoteService {

  @Headers({"Content-Type: application/json", "Accept: application/json"})
  @RequestLine("GET /mock/isLive?a={a}&b={b}")
  Object getAdd(@Param("a") int a, @Param("b") int b);
}
