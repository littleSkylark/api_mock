package life.gugugu.api.mock.ctrl;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * @author chances(YangPengLei)
 * @date 2020/9/3
 */
@RestController
@RequestMapping("/mock")
public class LiveController {
  @GetMapping("/isLive")
  @ResponseBody
  public Object isLive(HttpServletRequest request) throws InterruptedException {
    Thread.sleep(1100);
    return ResponseEntity.ok(
        new Object() {
          public String queryString = request.getQueryString();
          public String requestURL = request.getRequestURL().toString();
          public String requestURI = request.getRequestURI();
          public String servletPath = request.getServletPath();
          public String contextPath = request.getContextPath();
          public String pathInfo = request.getPathInfo();
          public String pathTranslated = request.getPathTranslated();
        });
  }
}
