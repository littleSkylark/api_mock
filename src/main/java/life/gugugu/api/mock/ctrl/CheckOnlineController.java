package life.gugugu.api.mock.ctrl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chances(YangPengLei)
 * @date 2019/12/4
 */
@RestController
@RequestMapping("/mock/api/checkOnline")
public class CheckOnlineController {
  private static final Logger log = LoggerFactory.getLogger(CheckOnlineController.class);

  @RequestMapping("/series/{code}.json")
  public Map<String, Object> checkSeries(@PathVariable String code) {
    return mockHandle(code);
  }

  @GetMapping("/vod/{code}.json")
  public Map<String, Object> checkVod(@PathVariable String code) {
    return mockHandle(code);
  }

  /**
   * 结尾双偶数返回-1
   *
   * @param code
   * @return
   */
  private Map<String, Object> mockHandle(String code) {
    Map<String, Object> map = new HashMap<>(4);
    int status = 200;
    String c = code.substring(code.length() - 1);
    if (check(c)) {
      if (code.length() < 2) {
        status = -1;
      } else {
        c = code.substring(code.length() - 2, code.length() - 1);
        if (check(c)) {
          status = -1;
        }
      }
    }
    map.put("status", status);
    map.put("code", code);
    map.put("msg", "mock api. code以双偶数结尾status返回-1，其他返回200");
    return map;
  }

  private boolean check(String c) {
    try {
      int value = Integer.parseInt(c);
      if (value % 2 == 0) {
        return true;
      }
    } catch (NumberFormatException e) {
      log.warn("format error:{}", c);
    }
    return false;
  }
}
