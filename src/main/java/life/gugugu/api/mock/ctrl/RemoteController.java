package life.gugugu.api.mock.ctrl;

import life.gugugu.service.RemoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author chances(YangPengLei)
 * @date 2021/1/22
 */
@RequestMapping("/mock/remote")
@Controller
public class RemoteController {

  @Autowired RemoteService remoteService;

  @RequestMapping("live")
  @ResponseBody
  public Object live() {
    return remoteService.getAdd(1, 2);
  }
}
