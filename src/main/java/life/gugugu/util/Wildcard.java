package life.gugugu.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

/**
 * @author chances(YangPengLei)
 * @date 2021/3/30
 */
public class Wildcard {
  private static final Pattern PATTERN = Pattern.compile("\\*{2,}");
  private final String[] patterns;
  private final boolean ignoreCase;

  public Wildcard(String wildcard, boolean ignoreCase) {
    if (StringUtils.isEmpty(wildcard)) {
      throw new IllegalArgumentException("规则需至少包含一个字符");
    }
    wildcard = PATTERN.matcher(wildcard).replaceAll("*");
    if (ignoreCase) {
      wildcard = wildcard.toLowerCase();
    }
    this.patterns = StringUtils.splitPreserveAllTokens(wildcard, '*');
    this.ignoreCase = ignoreCase;
  }

  public boolean isMatch(String s) {
    if (s == null) {
      return false;
    }
    if (ignoreCase) {
      s = s.toLowerCase();
    }
    int length = s.length();
    // todo
    return false;
  }

  private boolean charMatch(char c, char wildcard) {
    return c == wildcard || wildcard == '?';
  }
}
