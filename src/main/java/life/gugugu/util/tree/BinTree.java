package life.gugugu.util.tree;

import lombok.Data;

import java.util.Iterator;
import java.util.Stack;

/**
 * @author chances(YangPengLei)
 * @date 2020/1/6
 */
@Data
public class BinTree<E> implements Tree<E> {

  protected BinTreeNode<E> root;

  @Override
  public int high() {
    if (root == null) {
      return 0;
    }
    return root.high();
  }

  void traverse() {
    traverse(root);
  }

  void traverse(BinTreeNode<E> node) {
    if (node == null) {
      return;
    }
    System.out.println(node.getEle());
    traverse(node.getLTree());
    traverse(node.getRTree());
  }

  @Override
  public Iterable<E> preOrderTraverse() {
    return () ->
        new Iterator<E>() {
          private Stack<BinTreeNode<E>> stack = new Stack<>();

          {
            push(root);
          }

          public void push(BinTreeNode<E> node) {
            if (node != null) {
              stack.push(node);
            }
          }

          @Override
          public boolean hasNext() {
            return !stack.isEmpty();
          }

          @Override
          public E next() {
            BinTreeNode<E> treeNode = stack.pop();
            push(treeNode.rTree);
            push(treeNode.lTree);
            return treeNode.ele;
          }
        };
  }

  @Override
  public Iterable<E> inOrderTraverse() {
    return () ->
        new Iterator<E>() {
          private Stack<BinTreeNode<E>> stack = new Stack<>();

          {
            push(root);
          }

          public void push(BinTreeNode<E> node) {
            while (node != null) {
              stack.push(node);
              node = node.lTree;
            }
          }

          @Override
          public boolean hasNext() {
            return !stack.isEmpty();
          }

          @Override
          public E next() {
            BinTreeNode<E> treeNode = stack.pop();
            push(treeNode.rTree);
            return treeNode.ele;
          }
        };
  }

  @Override
  public Iterable<E> postOrderTraverse() {
    return () ->
        new Iterator<E>() {
          private Stack<BinTreeNode<E>> stack = new Stack<>();
          private Stack<Integer> flags = new Stack<>();

          {
            push(root);
          }

          public void push(BinTreeNode<E> node) {
            while (node != null) {
              stack.push(node);
              node = node.lTree;
            }
          }

          @Override
          public boolean hasNext() {
            return !stack.isEmpty();
          }

          @Override
          public E next() {
            BinTreeNode<E> treeNode = stack.peek();
            push(treeNode.rTree);
            return treeNode.ele;
          }
        };
  }

  @Data
  static class BinTreeNode<E> {
    private E ele;
    private BinTreeNode<E> lTree;
    private BinTreeNode<E> rTree;

    public int high() {
      int lHigh = 0, rHigh = 0;
      if (lTree != null) {
        lHigh = lTree.high();
      }
      if (rTree != null) {
        rHigh = rTree.high();
      }
      return (Math.max(rHigh, lHigh)) + 1;
    }
  }
}
