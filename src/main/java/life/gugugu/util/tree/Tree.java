package life.gugugu.util.tree;

import java.util.List;

/**
 * @author chances(YangPengLei)
 * @date 2020/1/6
 */
public interface Tree<E> {

  /**
   * 获取树的高度
   *
   * @return
   */
  int high();

  /**
   * 先序遍历
   *
   * @return
   */
  Iterable<E> preOrderTraverse();
  /**
   * 中序遍历
   *
   * @return
   */
  Iterable<E> inOrderTraverse();

  /**
   * 后序遍历
   *
   * @return
   */
  Iterable<E> postOrderTraverse();

  interface TreeNode<T> {
    /**
     * 返回当前节点
     *
     * @return
     */
    T getNode();

    /**
     * 获取全部子节点
     *
     * @return
     */
    List<TreeNode<T>> getChildren();
  }
}
