package life.gugugu.config;

import feign.Logger;
import feign.Request;
import feign.Retryer;
import feign.hystrix.HystrixFeign;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import life.gugugu.service.RemoteService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * @author chances(YangPengLei)
 * @date 2021/1/22
 */
@Slf4j
@Configuration
public class BeanConfig {
  @Bean
  public RemoteService remoteService() {
    return HystrixFeign.builder()
        .encoder(new JacksonEncoder())
        .decoder(new JacksonDecoder())
        .logger(new feign.slf4j.Slf4jLogger(RemoteService.class))
        .logLevel(Logger.Level.FULL)
        .retryer(new Retryer.Default(1000, 10000, 5))
        .requestInterceptor(
            template -> {
              // 设置token
              // requestTemplate.header("Authorization",
              //      "accessToken" + oauth2ResourceServerExecutor.getAccessToken());
            })
        .options(new Request.Options(2000, TimeUnit.SECONDS, 3500, TimeUnit.SECONDS, true))
        // .target(RemoteService.class, "http://220.123.23.23:8090/");
        .target(RemoteService.class, "http://localhost:8088/");
  }
}
