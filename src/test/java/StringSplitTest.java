import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

/**
 * 测试split效率
 *
 * @author chances(YangPengLei)
 * @date 2019/12/6
 */
class StringSplitTest {

  static final String[] TESTCASES = {"", "*", "**", "abc", "ab*cd", "*asd*m*as*CharSequence *??*"};
  static final int NUMBER = 1000_000;
  String[] split;

  @BeforeAll
  static void beforeAll() {
    System.out.println("init out");
  }

  @Test
  void test() {
    split = "ab2ab3ab".split("ab");
    this.out();
    split = "a2a3a".split("a");
    this.out();
    split = StringUtils.split("**", '*');
    this.out();
    split = StringUtils.splitPreserveAllTokens("**", '*');
    this.out();

    System.out.println("1".substring(1, 1));
  }

  @Test
  void split1() {
    Pattern compile = Pattern.compile("\\*");
    for (String testcase : TESTCASES) {
      IntStream.range(0, NUMBER).forEach(i -> split = compile.split(testcase));
      this.out();
    }
  }

  @Test
  void split2() {
    for (String testcase : TESTCASES) {
      IntStream.range(0, NUMBER).forEach(i -> split = testcase.split("\\*"));
      this.out();
    }
  }

  @Test
  void split3() {
    for (String testcase : TESTCASES) {
      IntStream.range(0, NUMBER)
          .forEach(i -> split = StringUtils.splitPreserveAllTokens(testcase, '*'));
      this.out();
    }
  }

  @Test
  void split4() {
    for (String testcase : TESTCASES) {
      IntStream.range(0, NUMBER)
          .forEach(
              t -> {
                int length = testcase.length();
                if (length == 0) {
                  split = ArrayUtils.EMPTY_STRING_ARRAY;
                } else {
                  final List<String> list = new ArrayList<>();
                  int begin = 0;
                  for (int i = 0; i < length; i++) {
                    if (testcase.charAt(i) == '*') {
                      list.add(testcase.substring(begin, i));
                      begin = i + 1;
                    }
                  }
                  list.add(testcase.substring(begin, length));
                  split = list.toArray(ArrayUtils.EMPTY_STRING_ARRAY);
                }
              });
      this.out();
    }
  }

  private void out() {
    System.out.println(split.length + ":" + Arrays.toString(split));
  }
}
