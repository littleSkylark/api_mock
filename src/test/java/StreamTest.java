import lombok.ToString;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * @author chances(YangPengLei)
 * @date 2021/2/9
 */
public class StreamTest {

  @ToString
  static class A {
    int a;

    A(int a) {
      this.a = a;
    }
  }

  public static void main(String[] args)
      throws NoSuchMethodException, InvocationTargetException, IllegalAccessException,
          NoSuchFieldException {
    FileDescriptor fileDescriptor = new FileDescriptor();
    Class<FileDescriptor> clazz = FileDescriptor.class;
    Method set = clazz.getDeclaredMethod("set", int.class);
    set.setAccessible(true);
    Object invoke = set.invoke(clazz, 3);
    Field handle = clazz.getDeclaredField("handle");
    handle.setAccessible(true);
    handle.set(fileDescriptor, invoke);
    PrintStream printStream = new PrintStream(new FileOutputStream(fileDescriptor));

    printStream.println("asb");
    Object[] aa = new String[0];
    Object[] aaa = new String[0][0];
    System.out.println(aa.getClass());
    System.out.println(aaa.getClass());
    System.out.println(aaa.getClass() == aa.getClass());

    List<A> collect =
        IntStream.range(0, 10).mapToObj(A::new).peek(a -> a.a++).collect(Collectors.toList());
    System.out.println(collect);
  }
}
