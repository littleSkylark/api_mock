import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author chances(YangPengLei)
 * @date 2021/1/28
 */
public class ThreadPoolTest {

  private static final ExecutorService EXECUTOR = Executors.newSingleThreadExecutor();

  public static void main(String[] args) {
    EXECUTOR.execute(
        () -> {
          System.out.println("error test");
          throw new RuntimeException();
        });
    EXECUTOR.submit(
        () -> {
          System.out.println("error test");
          throw new RuntimeException();
        });
  }
}
