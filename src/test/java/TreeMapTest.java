import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author chances(YangPengLei)
 * @date 2020/12/11
 */
public class TreeMapTest {

  @Data
  @AllArgsConstructor
  public static class User implements Comparable<User> {
    Long id;
    String name;

    @Override
    public int compareTo(User other) {
      return (int) (this.id - other.id);
    }
  }

  public static void main(String[] args) {
    Map<User, User> userMap = new HashMap<>();
    User user1 = new User(1L, "Marr");
    User user2 = new User(2L, "Tom");
    User user3 = new User(3L, "Slice");
    userMap.put(user1, user1);
    userMap.put(user2, user2);
    userMap.put(user3, user3);
    System.out.println(userMap.keySet());

    TreeMap<User, User> treeMap =
        new TreeMap<>(
            (u1, u2) -> {
              System.out.println(u1 + ", " + u2);
              return (int) (u1.id - u2.id);
            });
    //如果两个节点相同会被覆盖
    treeMap.putAll(userMap);
    System.out.println(treeMap.keySet());

    user1.id = 4L; // 红黑树被破坏
    treeMap.put(user1, user1);
    System.out.println(treeMap.keySet());

    TreeMap<User, User> treeMap1 = new TreeMap<>(userMap);
    System.out.println(treeMap1.keySet());
  }
}
