import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousFileChannel;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Objects;
import java.util.concurrent.Future;

/**
 * @author chances(YangPengLei)
 * @date 2021/3/11
 */
public class AsyncFileTest {

  static ByteBuffer buffer = ByteBuffer.allocate(100);

  private static Future<Integer> read() throws IOException, URISyntaxException {
    URI uri =
        Objects.requireNonNull(AsyncFileTest.class.getClassLoader().getResource("test.json"))
            .toURI();
    AsynchronousFileChannel asyncFileChannel =
        AsynchronousFileChannel.open(Paths.get(uri), StandardOpenOption.READ);
    return asyncFileChannel.read(buffer, 0);
  }

  public static void main(String[] args) throws Exception {
    Future<Integer> read = read();
    Integer i = read.get();
    System.out.println(i);
    System.out.println(new String(buffer.array(), 0, i));
  }
}
