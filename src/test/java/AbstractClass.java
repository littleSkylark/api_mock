/**
 * @author chances(YangPengLei)
 * @date 2021/1/25
 */
public abstract class AbstractClass {
  public abstract String toString();
}

class A extends AbstractClass {

  @Override
  public String toString() {
    return null;
  }
}
