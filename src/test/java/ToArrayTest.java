import java.util.Arrays;
import java.util.List;

/**
 * @author chances(YangPengLei)
 * @date 2021/1/27
 */
public class ToArrayTest {

  public static void main(String[] args) {
    List<Integer> list = Arrays.asList(1, 2, 3);
    /*
    	  有两种样式可以将集合转换为数组：
    	  使用预先调整大小的数组（如c.toArray（new String[c.size（）]）
    	  或使用空数组（如c.toArray（new String[0]）。

    在较旧的Java版本中，建议使用预大小的数组，因为创建适当大小的数组所需的反射调用非常慢。
    然而，由于openjdk6的最新更新，这个调用是内部化的，使得空数组版本的性能与以前的版本相同，有时甚至更好
    。另外，传递预大小的数组对于并发或同步的集合来说是危险的，因为大小和toArray调用之间可能存在数据争用，
    如果在操作过程中同时收缩了集合，则可能会导致数组末尾出现额外的空值。

    这种检查允许遵循统一的风格：要么使用空数组（在现代Java中建议这样做），
    要么使用预先调整大小的数组（在较旧的Java版本或非基于热点的jvm中可能更快）。
    	   */
    Integer[] array = list.toArray(new Integer[0]);
    System.out.println(Arrays.toString(array));
  }
}
