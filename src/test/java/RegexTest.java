import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chances(YangPengLei)
 * @date 2021/3/30
 */
public class RegexTest {

  @Test
  void test() {
    Pattern pattern = Pattern.compile(".*");
    Assertions.assertThrows(
        NullPointerException.class,
        () -> {
          Matcher matcher = pattern.matcher(null);
          while (matcher.find()) {
            System.out.println(matcher.group());
          }
        });
  }
}
