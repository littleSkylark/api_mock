package life.gugugu.api.mock;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
class ApiMockApplicationTests {
  private MockMvc mockMvc;
  @Autowired private WebApplicationContext wac;

  @BeforeEach
  public void setup() {
    this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).dispatchOptions(true).build();
  }

  @Test
  void test() throws Exception {
    String code = "迪迪234";
    ResultActions resultActions =
        mockMvc.perform(
            get("/mock/api/checkOnline/series/" + code + ".json")
                .characterEncoding("utf-8")
                .accept(MediaType.APPLICATION_JSON)
                .header("charset", "utf-8"));
    resultActions.andReturn().getResponse().setCharacterEncoding("utf-8");
    resultActions
        .andDo(MockMvcResultHandlers.print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("code").exists())
        .andExpect(jsonPath("code", is(code)))
        .andReturn();
  }
}
