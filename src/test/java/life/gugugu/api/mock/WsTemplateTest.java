package life.gugugu.api.mock;

import org.junit.jupiter.api.Test;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.util.ClassUtils;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.WebServiceMessageSender;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * @author chances(YangPengLei)
 * @date 2020/1/3
 */
public class WsTemplateTest {

  @Test
  void contextLoads() throws Exception {
    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
    marshaller.setPackagesToScan(ClassUtils.getPackageName(ResultNotify.class));
    marshaller.afterPropertiesSet();
    WebServiceTemplate wsTemplate = new WebServiceTemplate(marshaller);
    WebServiceMessageSender[] senders = wsTemplate.getMessageSenders();
    for (WebServiceMessageSender sender : senders) {
      System.out.println(sender.getClass().getName());
    }
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlRootElement(name = "ResultNotify", namespace = "iptv")
  public static class ResultNotify implements Serializable {

    public static final int RESULT_SUCCESS = 0;

    @XmlElement(required = true, name = "CSPID")
    private String cspID;

    @XmlElement(required = true, name = "LSPID")
    private String lspID;

    @XmlElement(required = true, name = "CorrelateID")
    private String correlateID;

    @XmlElement(required = true, name = "CmdResult")
    private int cmdResult;

    @XmlElement(required = true, name = "ResultFileURL")
    private String resultFileURL;

    public String getCspID() {
      return cspID;
    }

    public void setCspID(String cspID) {
      this.cspID = cspID;
    }

    public String getLspID() {
      return lspID;
    }

    public void setLspID(String lspID) {
      this.lspID = lspID;
    }

    public String getCorrelateID() {
      return correlateID;
    }

    public void setCorrelateID(String correlateID) {
      this.correlateID = correlateID;
    }

    public int getCmdResult() {
      return cmdResult;
    }

    public void setCmdResult(int cmdResult) {
      this.cmdResult = cmdResult;
    }

    public String getResultFileURL() {
      return resultFileURL;
    }

    public void setResultFileURL(String resultFileURL) {
      this.resultFileURL = resultFileURL;
    }

    public boolean isSuccess() {
      return cmdResult == 0;
    }
  }
}
