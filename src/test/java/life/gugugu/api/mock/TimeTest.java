package life.gugugu.api.mock;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author chances(YangPengLei)
 * @date 2020/10/7
 */
public class TimeTest {
  public static void main(String[] args) throws IOException {
    String cmd = " cmd /c date ";
    LocalDate date = LocalDate.now().plusDays(1);
    Runtime.getRuntime().exec(cmd + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
  }
}
