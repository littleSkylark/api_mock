package life.gugugu.api.mock;
/**
 * @author chances(YangPengLei)
 * @date 2019/12/25
 */
interface Comparator {
  boolean compare(int i, int j);

  Comparator ASCENDING = (i, j) -> i < j;
  Comparator DESCENDING = (i, j) -> i > j;
}

class MyTest {
  public void test() {
    // use like this
    System.out.println(Comparator.ASCENDING.compare(1, 2));
  }
}
