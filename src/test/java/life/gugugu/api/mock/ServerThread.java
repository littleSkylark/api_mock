package life.gugugu.api.mock;

import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
/**
 *   Here I provide an idea, which can be solved by event monitoring (Observer Pattern). <br>
 *   Can separate publishers into a separate class
 */
public class ServerThread implements Runnable {

  // Used for event subscription (CopyOnWriteArrayList is not the best implementation, please adjust
  // according to project needs)
  private static final List<ServerThread> LISTENERS = new CopyOnWriteArrayList<>();

  public ServerThread(Socket socket, int num) {
    // do something ...
    LISTENERS.add(this);
  }

  @Override
  public void run() {
    while (true) {
      String command;
      try {
        command = readString(); // method that reads input from Client
        // Do actions based on command
        if ("send".equals(command)) {
          ServerThread.publish(new SendEvent(command));
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
    // Close streams and socket
  }

  private String readString() {
    return null;
  }

  /**
   * event publishing method
   *
   * @param event
   */
  public static void publish(SendEvent event) {
    LISTENERS.forEach(listener -> listener.listen(event));
  }

  /**
   * Event processing method
   *
   * @param event
   */
  public void listen(SendEvent event) {
    // to something ...
  }

  class SendEvent {
    SendEvent(String command) {
      System.out.println(command);
    }
  }
}
