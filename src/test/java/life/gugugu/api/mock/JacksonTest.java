package life.gugugu.api.mock;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * @author chances(YangPengLei)
 * @date 2019/12/30
 */
public class JacksonTest {

  URL url = this.getClass().getClassLoader().getResource("test.json");

  @Test
  void test() throws IOException {
    JsonFactory jsonFactory = new MappingJsonFactory();
    JsonParser parser = jsonFactory.createParser(url);
    JsonToken current = parser.nextToken();
    if (current != JsonToken.START_OBJECT) {
      return;
    }
    while (parser.nextToken() != JsonToken.END_OBJECT) {
      String fieldName = parser.getCurrentName();
      current = parser.nextToken();
      if (fieldName.equals("features") || fieldName.equals("type")) {
        if (current == JsonToken.START_ARRAY) {
          while (parser.nextToken() != JsonToken.END_ARRAY) {
            JsonNode node = parser.readValueAsTree();
            // And now we have random access to everything in the object
            System.out.println("node: " + node.get("geometry"));

            // here I want to convert and create another updated file of [Lat, Long]

          }
        } else if (current == JsonToken.VALUE_STRING) {
          System.out.println("field1: " + parser.readValueAs(String.class));
        } else {
          parser.skipChildren();
        }
      }
    }
  }

  @Test
  void test2() throws IOException {
    ObjectMapper objectMapper = new ObjectMapper();
    FeatureCollection value = objectMapper.readValue(url, FeatureCollection.class);
    System.out.println(value);
  }

  @Data
  public static class FeatureCollection {
    private String type;
    private List<Feature> features;
  }

  @Data
  public static class Feature {
    private String type;
    private Geometry geometry;
  }

  @Data
  private static class Geometry {
    private String type;
    private String[][][][] coordinates;
  }
}
