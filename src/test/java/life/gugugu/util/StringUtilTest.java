package life.gugugu.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

/**
 * @author chances(YangPengLei)
 * @date 2021/3/25
 */
class StringUtilTest {
  private static Stream<Arguments> wildcardsMatchingTestcase() {
    Arguments[] arguments = {
      Arguments.of("a", new String[] {"b", "c"}, new boolean[] {false, false}),
      Arguments.of("?", new String[] {"cc", "c", "?"}, new boolean[] {false, true, true}),
      Arguments.of("*", new String[] {"das", "?c"}, new boolean[] {true, true}),
      Arguments.of(
          "wd*Ab", new String[] {"das", "*wd*ab", "wdSdkAbAb"}, new boolean[] {false, false, true}),
    };
    return Stream.of(arguments);
  }

  @ParameterizedTest
  @MethodSource("wildcardsMatchingTestcase")
  void wildcardsMatching(String wildcards, String[] sample, boolean[] matches) {
    for (int i = 0; i < sample.length; i++) {
      System.out.println(wildcards + "#" + sample[i] + "#" + matches[i]);
      Assertions.assertEquals(matches[i], StringUtil.wildcardMatching(wildcards, sample[i]));
    }
  }
}
